import datetime
import tensorflow as tf
import numpy as np
from dcgan.dcgan import DCGAN
from dcgan import utils


def test(ckpt_root, model, batch_size):
    X, Z, Lr = model.inputs()
    g_sample = model.sample(Z, reuse=False)
    sample_size = batch_size
    test_noise = utils.get_noise(sample_size, n_noise)

    saver = tf.train.Saver()

    with tf.Session() as sess:
        saver.restore(sess, ckpt_root)

        samples = sess.run(g_sample, feed_dict={Z: test_noise})
        date = datetime.datetime.now()
        title = 'samples/dcgan/%s.png' % date
        utils.save_samples(title, samples)
    np.savetxt('samples/dcgan/test_noise%s.txt' % date, test_noise, fmt='%2.5f', delimiter=', ')


if __name__ == '__main__':
    # set hyper parameters
    batch_size = 9
    n_noise = 100
    image_size = 64
    image_channels = 3
    learning_rate = 0.0002
    total_epochs = 20

    tf.reset_default_graph()

    model = DCGAN(batch_size, n_noise, image_size, image_channels)

    ckpt_root = './weights/dcgan/dcgan-19'

    with tf.Graph().as_default():
        test(ckpt_root, model, batch_size)

    print('Done')
